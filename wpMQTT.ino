#include <ESP8266WiFi.h>
//#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include "StringSplitter.h"

// WIFI
const char* ssid = "xxx";
const char* password = "xxxx";
// Mobile hotspot (for testing)
const char* ssid_hotspot = "xxxx";
const char* password_hotspot = "xxxx";

// MQTT
const char* mqtt_server = "xx.xx.xx.xx";    
const char* mqtt_username = "xx";
const char* mqtt_password = "xx";
const char* mqtt_client = "xxx";
const char* mqtt_out_topic = "xxx";

// the static IP address for the esp:
IPAddress ip(192, 168, 0, 192);               
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress DNS(192, 168, 0, 1);

// web server
ESP8266WebServer server(80);

// wifi client
WiFiClient espClient;

// mqtt client
PubSubClient mqttClient(espClient);

/**
 * variables
 */
 
long lastMsg = 0;
char msg[75];
int value = 0;

// define pins
const int led = LED_BUILTIN;
const int pir = 14; // wemos D1 mini pin D5 (ESP-8266 GPIO14)

// define status variables
int ledStatus = 0;
int pirStatus = 0;

long wpID = 0;

/**
 * functions
 */
 
int getPirStatus() {
  pirStatus = digitalRead(pir);
  Serial.println(" pirStatus = " + pirStatus);
  return pirStatus;
}

void handleRoot() {
  digitalWrite(led, 0);
  server.send(200, "text/plain", "Hello from esp8266!");
  digitalWrite(led, 1);
}

void handleGetPir() {
  digitalWrite(led, 0);
  server.send(200, "text/plain", "PIR = " + getPirStatus());
  digitalWrite(led, 1);
}

void handleNotFound() {
  digitalWrite(led, 0);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 1);
}

void callback(char* topic, byte* payload, unsigned int length) {
  String message;
  char charArray[length + 1];
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    charArray[i] = (char)payload[i];
  }
  charArray[length] = '\0';
  message = String(charArray);
  Serial.println();
  Serial.println(message);
  Serial.println();

  /*
  if (strcmp(topic,"wcIn/led")==0) {
    // Switch on the LED if an 1 was received as first character
    if ((char)payload[0] == '1') {
      digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on, by making the voltage LOW
      // (Note that LOW is the voltage level
      // but actually the LED is on; this is because
      // it is active low on the ESP-01)
      ledStatus = 1;
    } else if ((char)payload[0] == '0') {
      digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off, by making the voltage HIGH
      ledStatus = 0;
    }
  } else if (strcmp(topic,"wcIn/relay")==0) {
    if (message.substring(0, 2) == "r0") {
      Serial.println("relay OFF");
      digitalWrite(relay, 0);
      relayStatus = 0;
    } else if (message.substring(0, 2) == "r1") {
      Serial.println("relay ON");
      digitalWrite(relay, 1);
      relayStatus = 1;
    }
  } else if (strcmp(topic,"wcIn/current")==0) {
    if ((char)payload[0] == 'C') {
      getCurrent().toCharArray(msg, 75);
      mqttClient.publish("wcOut/current", msg);
    }
  }
  */
}

void reconnect() {
  // Loop until we're reconnected
  while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqttClient.connect(mqtt_client, mqtt_username, mqtt_password)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      String(mqtt_client + String(" online")).toCharArray(msg, 75);
      mqttClient.publish("red/wpOut", msg);
      // ... and resubscribe
      mqttClient.subscribe("red/wpIn/#");
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

/**
 * setup
 */
 
void setup() {
  pinMode(led, OUTPUT);
  pinMode(pir, INPUT);
  digitalWrite(led, 0);
  
  wpID = ESP.getChipId();
  Serial.begin(115200);
  //WiFi.config(ip, gateway, subnet, DNS);  // use this for static IP
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.println("booting...");

  int wifi_choice = 0;
  // Wait for wifi connection
  delay(5000);
  while (WiFi.status() != WL_CONNECTED) {
    if (wifi_choice == 0) {
      WiFi.begin(ssid, password);
      wifi_choice = 1;
    } else {
      WiFi.begin(ssid_hotspot, password_hotspot);
      wifi_choice = 0;
      delay(5000);
    }
    delay(10000);
    Serial.print(".");
  }

  // alternative way for waiting for wifi connection
//  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
//    Serial.println("Connection Failed! Rebooting...");
//    delay(5000);
//    ESP.restart();
//  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  
  Serial.println("");
  Serial.print("Wifi connected to ");
  Serial.println(WiFi.SSID());
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Chip ID: ");
  Serial.println(wpID);
  Serial.print("Flash Chip ID: ");
  Serial.println(ESP.getFlashChipId());

//  if (MDNS.begin("esp8266")) {
//    Serial.println("MDNS responder started");
//  }

  // configure HTTP REST endpoint
  server.on("/", handleRoot);
  server.on("/pir", handleGetPir);
  server.on("/inline", [](){
    server.send(200, "text/plain", "this works as well");
  });
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");

  // configure MQTT client
  mqttClient.setServer(mqtt_server, 1883);
  mqttClient.setCallback(callback);

  digitalWrite(led, 1);
}


/**
 * loop
 */

void loop() {

  // handle OTA
  ArduinoOTA.handle();

  // handle HTTP server
  server.handleClient();

  // connect/handle MQTT client
  if (!mqttClient.connected()) {
    reconnect();
  }
  mqttClient.loop();

  long now = millis();
  if (now - lastMsg > 5000) {
    lastMsg = now;
    ++value;
    digitalWrite(led, 0); // led ON

    // publish PIR status
    String(getPirStatus()).toCharArray(msg, 75);
    Serial.print("Publish message for PIR: ");
    Serial.println(msg);
    mqttClient.publish(mqtt_out_topic, msg);

    Serial.println("-----------------");
    Serial.println();
    digitalWrite(led, 1); // led OFF
  }
  
}
